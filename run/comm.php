<?php
//共用變數/func

//可用字,故意打亂
//$kw = array(
//	"6", "y", "h", "n", "7", "u", "j", "m", "8", "i", "k", "9", "o", "l", "0", "p", "1", "q", "a", "z", "2", "w", "s", "x", "3", "e", "d", "c", "4", "r", "f", "v", "5", "t", "g", "b"
//);
//asort($kw);//如果想用有序可用字表
function getDB()
{
	//資料庫連線

	require('db.php');
	$db = new PDO($dbset['dns'], $dbset['dbuser'], $dbset['dbpwd']);
	$db->query('set names utf8;');
	return  $db;
}



function array2get($arr, $idx, $def)
{
	//從陣列中取變數,如不存在則返回$def

	try {
		if (isset($arr[$idx])) {
			return $arr[$idx];
		}
	} catch (Exception $th) {
	}


	return $def;
}



function loopURLdec($url = "")
{
	//url循環解碼

	$tr = trim($url); //輸入參數去空白

	$out = $tr;
	//$u2 = $src;
	$loop = false; //是否需要循環解碼
	do {
		$dec = urldecode($out);
		$loop = $dec !== $out; //解碼成功則$dec != $out , 所以loopdec=true
		if ($loop) {
			$out = trim($dec); //覆蓋並去空白
		}
	} while ($loop); //循環解碼

	//到此,循環解碼完成
	$out = strtolower(trim($out)); //再次去空白,轉小寫
	//echo "u1='$u1'";
	return $out;
}

function err_msg($msg = "")
{
	return array(
		"stat" => "error",
		"err_msg" => $msg
	);
}

function para2arr($str = "")
{
	//網址查詢參數轉陣列
	$para = array();
	$arr = explode('&', $str); //;

	foreach ($arr as $v) {
		$arr2 = explode('=', $v);
		if (count($arr2) > 1) { //避免kv切割是失敗結果

			$k2 = trim(array2get($arr2, 0, ""));

			if (strlen($k2) > 0) { //有效key
				$para[$k2] = trim(array2get($arr2, 1, ""));
			}
		}
	}

	ksort($para); //排序參數key

	return $para;
}


function ver54()
{
	//是否5.4版本
	try {
		$a = explode(".", phpversion());
		$v = floatval($a[0] . "." . $a[1]);

		return $v > 5.3;
	} catch (Exception $exc) {
		//echo $exc->getTraceAsString();
	}
	return false;
}


function unicode2cjk($str)
{
	//unicode字串轉成正常中日韓字串
	return preg_replace("#\\\u([0-9a-f]+)#ie", "iconv('UCS-2', 'UTF-8', pack('H4', '\\1'))", $str);
}


function arr2json($arr, $fname = '')
{
	//json輸出
	header('Content-Type:application/json;charset=utf-8');

	if (strlen($fname) > 0) {
		header("Content-Disposition:filename=i35_$fname.json");
	}

	if (ver54()) {
		echo json_encode($arr, JSON_UNESCAPED_UNICODE);
	} else {
		echo unicode2cjk(json_encode($arr));
	}
}

function getIP($arr)
{
	//用戶IP查詢
	$ip = "";

	$ips = array();

	foreach (array(
		"HTTP_CLIENT_IP",
		"HTTP_X_FORWARDED_FOR",
		"HTTP_X_FORWARDED",
		"HTTP_X_CLUSTER_CLIENT_IP",
		"HTTP_FORWARDED_FOR",
		"HTTP_FORWARDED",
		"REMOTE_ADDR",
		"HTTP_VIA"
	) as $v) {
		$ip = trim(array2get($arr, $v, ""));

		$ips[] = $ip;

		if (strlen($ip) > 0) {
			break;
		}
	}

	//print_r($ips);

	return $ip;
}

function paras2str($arr = array())
{

	if (count($arr) < 1) {
		return "";
	}

	$arr2 = array();
	$arr2[] = "?";
	$was = false;
	foreach ($arr as $k => $v) {
		if ($was) {
			$arr2[] = "&";
		}
		$arr2[] = $k;
		$arr2[] = "=";
		$arr2[] = $v;
		$was = true;
	}

	return implode("", $arr2);
}

function getUrlsData($url = "", $protocol = "", $paras = "", $db = null)
{
	if (!isset($db)) {
		$db = getDB();
	}

	$arr = array();
	$sth = $db->prepare('SELECT * FROM urls WHERE url = ? AND paras = ? AND protocol = ? LIMIT 1 ;');
	$sth->execute(array($url,  $paras, $protocol));
	if ($q = $sth->fetch(PDO::FETCH_ASSOC)) {
		return $q;
	}
	return $arr;
}

function save_history($t, $ip, $msg, $db = null)
{
	if (!isset($db)) {
		$db = getDB();
	}
	//紀錄這是哪個IP在什麼時候做什麼事情
	$sth = $db->prepare(' INSERT INTO shorturl.history_log (datime, ip, log) VALUES (?,?,?) ');
	$sth->execute(array($t, $ip, $msg));
}

function shortURL2long($short, $db = null)
{
	if (!isset($db)) {
		$db = getDB();
	}

	//SELECT * FROM urls WHERE urls.short = ? LIMIT 1
	$arr = array();
	$sth = $db->prepare(' SELECT * FROM urls WHERE urls.short = ? LIMIT 1 ');
	$sth->execute(array($short));
	if ($q = $sth->fetch(PDO::FETCH_ASSOC)) {
		return $q;
	}
	return $arr;
}
