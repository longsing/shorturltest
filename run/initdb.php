<?php
//初始化資料庫



function initDB()
{

	$sql = "SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

DROP TABLE IF EXISTS `history_log`;
CREATE TABLE `history_log`  (
  `_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '流水號',
  `datime` double(20, 3) NULL DEFAULT NULL COMMENT '發生時間',
  `ip` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '來源IP',
  `log` varchar(4000) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '發生什麼事',
  PRIMARY KEY (`_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 0 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '使用紀錄,包含建立網址,重複建立,使用網址' ROW_FORMAT = Dynamic;


DROP TABLE IF EXISTS `urls`;
CREATE TABLE `urls`  (
  `_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '流水號',
  `protocol` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '協定',
  `url` varchar(4096) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '來源網址不含參數',
  `paras` varchar(4096) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '網址的參數,存入前須排序參數',
  `short` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '短網址',
  PRIMARY KEY (`_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 0 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '原始網址與短網址' ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;";



	require_once('comm.php');

	$db = getDB();
	$sth = $db->prepare($sql);
	$sth->execute();

	echo "ok";
}

initDB();
