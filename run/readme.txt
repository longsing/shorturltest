
0.
以下安裝與啟動說明,如無法完成,請聯繫開發人員,
也可以使用執行中的範例:
https://i35i35.ddns.net/ShortURL/
此範例包含nginxurl rewrite處理(nginx.conf)

1.
你需要下載或安裝php,並且到安裝目錄執行此命令:
php-cgi.exe -b 127.0.0.1:1008;
並且保持這個程式執行中.

2.
你可以使用iis/nginx/apache...等各種http服務架設,並且設定php的對應port=1008

3.
你的http服務需要url rewrite的功能
設定好你的網址,讓 "你的網址/任意文字" 的任意文字部分轉為
"你的網址/trn.php?q=任意文字"

4.
db.php 是資料庫連線的基本資料,這邊請填入IP/帳號/密碼
如果你沒有資料庫,你可以使用已經在線上的資料庫服務

5.
initdb.php 是初始化資料庫,初始化之前請確認db.php的帳號密碼有建立刪改資料庫的權限
或是選擇使用shorturl.sql初始化資料庫.

6.
到你的首頁(打開index.htm),開始使用短網址服務
也可以使用執行中的範例:
https://i35i35.ddns.net/ShortURL/