<?php

require_once('comm.php');

//$url=$_POST["url"];

function cov_ok($url)
{
	return array(
		"act" => "cov",
		"stat" => "ok",
		"url" => "$url" //"trn.php?q=$url"
	);
}

function cov_err($msg)
{
	return array(
		"act" => "cov",
		"stat" => "error",
		"err_msg" => $msg
	);
}

function cov_innerr($msg)
{
	return array(
		"act" => "cov",
		"stat" => "innerError",
		"err_msg" => $msg
	);
}

function cov($url = "")
{
	$t = microtime(true);

	if (strlen($url) > 4096) {
		$msg = "超過4096字,拒絕服務";
		save_history($t, getIP($_SERVER), $msg);
		return cov_err($msg);
	}

	//return cov_innerr($url);

	//echo "url='$url'";
	//return;
	$err1 = "請輸入有效網址";

	switch (stripos($url, '://')) {
		case false:
			//網址異常
		case 0:
			//沒有協定
			$msg = "沒有協定";
			save_history($t, getIP($_SERVER), $msg);
			return cov_err($msg);
			break;

		default:
			//協定存在,網址未知,往下處理
			break;
	}

	$arr = explode('://', $url);

	if (count($arr) < 2) { //異常網址
		$msg = "沒有網址";
		save_history($t, getIP($_SERVER), $msg);
		return cov_err($msg);
	}

	$protocol = trim($arr[0]);

	if (strlen($protocol)  < 1) { //協定不存在
		$msg = "協定長度為0";
		save_history($t, getIP($_SERVER), $msg);
		return cov_err($msg);
	}

	$url = array2get($arr, 1, "");
	$url = loopURLdec($url); //循環解碼+去空白+轉小寫;

	if (strlen($url) < 1) { //網址無效
		save_history($t, getIP($_SERVER), $err1);
		return cov_err($err1);
	}

	$para = array();

	switch (stripos($url, '?')) {
		case false:
			//沒有參數,不處理
			break;

		case 0: //異常網址
			$msg = "有參數沒網址";
			save_history($t, getIP($_SERVER), $msg);
			return cov_err($msg);
			break;

		default:
			// n > 0 是有效參數,處理
			$arr2 = explode('?', $url);
			$url = $arr2[0];

			$para = para2arr(array2get($arr2, 1, "")); //網址查詢參數,可能為空陣列
			break;
	}

	//$url2 = $url . paras2str($para);

	$db = getDB();

	//getIP($_SERVER)

	//$q = strtolower(trim($_GET["q"])); //輸入參數,去空白,轉小寫.

	$paras = paras2str($para);

	$db->beginTransaction();
	//$db->rollBack();//取消
	//$db->commit();//完成

	$q1 = getUrlsData($url, $protocol, $paras, $db);
	//print_r($q1);

	if (count($q1) > 0) {
		//已經存在
		$db->rollBack(); //結束交易

		$b36 = array2get(
			$q1,
			"short",
			""
		);

		save_history($t, getIP($_SERVER), "was shorturl , return this : $b36");

		return cov_ok(
			$b36
		); //直接回傳短網址

	}

	//至此,需要寫入
	$sth = $db->prepare('INSERT INTO shorturl.urls (protocol, url, paras ) VALUES (?, ?, ?) ;');
	$sth->execute(array($protocol, $url, $paras));
	$insCnt = $sth->rowCount(); //影響筆數
	//替代可用lastInsertId 
	if ($insCnt < 1) {
		//寫入無效,錯誤結束
		//$db->commit();
		$db->rollBack(); //結束交易
		$msg = "資料庫寫入筆數為0";
		save_history($t, getIP($_SERVER), $msg);
		return cov_innerr($msg);
	}

	//至此,寫入有效,更新短網址
	$q1 = getUrlsData($url, $protocol, $paras, $db); //這次的內容沒有短網址

	//print_r($q1);

	$id = array2get($q1, "_id", -1);
	if ($id < 0) {
		//沒有_id,結束
		//$db->commit();
		$db->rollBack(); //結束交易
		$msg = "資料庫寫入失敗:沒有_id";
		save_history($t, getIP($_SERVER), $msg);
		return cov_innerr($msg);
	}

	//至此,有id可更新
	$b36 = base_convert($id, 10, 36); //將數字轉成0~9+a~z的字串

	//UPDATE shorturl.urls SET short = ? WHERE _id = ?
	$sth = $db->prepare(' UPDATE shorturl.urls SET short = ? WHERE _id = ? ;');
	$sth->execute(array($b36, $id));
	$insCnt = $sth->rowCount(); //影響筆數

	if ($insCnt < 1) {
		//寫入無效,錯誤結束
		//$db->commit();
		$db->rollBack(); //回退並結束交易
		$msg = "資料庫更新筆數為0";
		save_history($t, getIP($_SERVER), $msg);
		return cov_innerr($msg);
	}


	$db->commit(); //至此短網址產出與更新完成

	//紀錄這是哪個IP在什麼時候做什麼事情
	save_history($t, getIP($_SERVER), "shorturl.urls._id=$id");
	//$sth = $db->prepare(' INSERT INTO shorturl.history_log (datime, ip, log) VALUES (?,?,?) ');
	//$sth->execute(array($t, getIP($_SERVER), "shorturl.urls._id=$id"));

	return cov_ok($b36);
}

arr2json(
	cov(
		array2get(
			$_POST,
			"url",
			""
		)
	)
);
